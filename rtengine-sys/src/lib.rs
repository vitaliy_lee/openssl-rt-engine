use std::marker;

use libc::{c_int, c_void};

use pkcs11::types::{CK_FUNCTION_LIST_PTR, CK_OBJECT_HANDLE, CK_SESSION_HANDLE};

// TODO:
// enum {
//     rt_eng_nid_gost28147_cfb = NID_id_Gost28147_89,
//     rt_eng_nid_gost28147_paramset_z_cbc = NID_gost89_cbc,
//     rt_eng_nid_gost28147_paramset_a_cnt = NID_gost89_cnt,
//     rt_eng_nid_gost28147_paramset_z_cnt = NID_gost89_cnt_12,
//     rt_eng_nid_gost28147_paramset_z_ecb = NID_gost89_ecb,
//     rt_eng_nid_gost28147_paramset_a_mac = NID_id_Gost28147_89_MAC,
//     rt_eng_nid_gost28147_paramset_z_mac = NID_gost_mac_12
// };

#[repr(C)]
pub struct Session {
    _data: [u8; 0],
    _marker: marker::PhantomData<(*mut u8, marker::PhantomPinned)>,
}

pub type MutexFunc = extern "C" fn(mutex: *mut c_void) -> c_int;

/// Структура для хранения мьютекса и его функций.
#[repr(C)]
pub struct Mutex {
    pub mutex: *mut c_void,

    /// Функция блокирования мьютекса.
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub lock: MutexFunc,

    /// Функция освобождения мьютекса.
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub unlock: MutexFunc,
}

#[cfg_attr(
    any(target_os = "macos", target_os = "ios"),
    link(name = "rtengine", kind = "framework")
)]
#[cfg_attr(
    any(target_os = "linux", target_os = "android", target_os = "freebsd"),
    link(name = "rtengine")
)]
extern "C" {
    /// Загрузка rtengine в OpenSSL.
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub fn rt_eng_load_engine() -> c_int;

    /// Удаление rtengine из OpenSSL. Возвращает ошибку, если остались ссылки на engine.
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub fn rt_eng_unload_engine() -> c_int;

    /// Получение экземпляра rtengine. Если функция вызывается до вызова rt_eng_load_engine, поведение не определено.
    /// Возвращает описатель rtengine.
    pub fn rt_eng_get0_engine() -> *mut openssl_sys::ENGINE;

    /// Функция создания сессии.
    ///
    /// # Аргументы
    ///
    /// * `function_list` - список функций, полученных от rtpkcs11ecp.
    /// * `handle` - описатель сессии, полученной от rtpkcs11ecp.
    /// * `hold` - параметр передачи прав управления сессией. При его равенстве 1, сессия будет закрыта средствами OpenSSL при удалении описателя. При его равенстве 0, сессия должна быть закрыта пользователем самостоятельно.
    /// * `mutex` - внешний мьютекс для управления сессией. Для использования внутреннего мьютекса необходимо передать null.
    ///
    /// Возвращает описатель обёрнутой сессии в случае успеха, null в случае ошибки.
    pub fn rt_eng_p11_session_wrap(
        function_list: CK_FUNCTION_LIST_PTR,
        handle: CK_SESSION_HANDLE,
        hold: c_int,
        mutex: *const Mutex,
    ) -> *mut Session;

    /// Функция удаления сессии.
    pub fn rt_eng_p11_session_free(session: *mut Session) -> c_void;

    /// Функция увеличения счётчика ссылок.
    ///
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub fn rt_eng_p11_session_up_ref(session: *mut Session) -> c_int;

    /// Функция блокирования сессии.
    ///
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub fn rt_eng_p11_session_lock(session: *mut Session) -> c_int;

    /// Функция освобождения сессии.
    ///
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub fn rt_eng_p11_session_unlock(session: *mut Session) -> c_int;

    /// Получение описателя ключевой пары.
    ///
    /// # Аргументы
    ///
    /// * `session` - описатель обёрнутой сессии.
    /// * `private_key` необязательный) -- описатель закрытого ключа, полученный от rtpkcs11ecp. Если для работы приложения отсутствует необходимость использовать закрытый ключ, то следует установить данный параметр в значение CK_INVALID_HANDLE.
    /// * `public_key` - описатель открытого ключа, полученный от rtpkcs11ecp.
    ///
    /// Вовзвращает описатель ключевой пары в случае успеха, null в случае ошибки.
    pub fn rt_eng_p11_key_pair_wrap(
        session: *mut Session,
        private_key: CK_OBJECT_HANDLE,
        public_key: CK_OBJECT_HANDLE,
    ) -> *mut openssl_sys::EVP_PKEY;

    /// Данная функция делает описатель ключевой пары недействительным.
    /// При использовании ключевой пары, находящейся на токене, могут возникать ситуации, при которых описатель объектов на токене становится более не актуальным, например при сбросе прав доступа к объектам на токене или при физическом удаление ключа из памяти токена. Во избежании неопределённого поведения описатель ключевой пары необходимо делать недействительным.
    ///
    /// # Аргументы
    ///
    /// * `pkey` - описатель ключевой пары.
    ///
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub fn rt_eng_p11_key_pair_invalidate(pkey: *mut openssl_sys::EVP_PKEY) -> c_int;

    /// Функция добавления в контекст хэширования PKCS11 сессии.
    /// Если в контекст была добавлена хотя бы одна сессия, хэш будет посчитан на токене.
    /// При добавлении нескольких сессий в один контекст хэш будет посчитан для каждой из них.
    /// При хэшировании на токене данные накапливаются в буфере до вызова final.
    ///
    /// # Аргументы
    ///
    /// * `ctx` - контекст хэширования.
    /// * `session` - описатель обёрнутой сессии.
    ///
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub fn rt_eng_md_add1_p11_session(
        ctx: *mut openssl_sys::EVP_MD_CTX,
        session: *mut Session,
    ) -> c_int;

    /// Включение механизма получения случайных чисел с токена.
    /// Для включения этого механизма функцию необходимо вызвать до регистрации rtengine.
    ///
    /// # Аргументы
    ///
    /// * `toggle` - включить/выключить.
    ///
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub fn rt_eng_rand_enable(engine: *mut openssl_sys::ENGINE, toggle: bool) -> c_int;

    /// Установка текущей сессии для получения энтропии.
    ///
    /// # Аргументы
    ///
    /// * `session` - описатель обёрнутой сессии.
    ///
    /// Возвращает 1 в случае успеха, 0 в случае ошибки.
    pub fn rt_eng_rand_set0_p11_session(session: *mut Session) -> c_int;
}
