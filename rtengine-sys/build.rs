fn main() {
    if cfg!(any(target_os = "macos", target_os = "ios")) {
        println!("cargo:rustc-link-search=framework=/Library/Frameworks");
    }

    if cfg!(any(target_os = "macos")) {
        println!("cargo:rustc-link-search=native=/usr/local/opt/openssl@1.1/lib");
    }
}
