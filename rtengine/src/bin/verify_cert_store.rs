use std::env;
use std::fs;
use std::path::Path;
use std::process;

use openssl_rt_engine::*;

fn verify_certificate_store<P: AsRef<Path>>(
    store_path: P,
) -> std::result::Result<(), Box<dyn std::error::Error>> {
    let store_path = store_path.as_ref();

    println!("Проверяется 📁\t {:?}", store_path);

    let trust_store = open_fs_trust_store(store_path)?;

    let certs = fs::read_dir(store_path.join("certs"))?;

    for entry in certs {
        let cert_path = entry?.path();
        let certificate = open_certificate(&cert_path)?;

        let valid = verify_certificate(&trust_store, &certificate)?;

        print!("Проверен 📄\t {:?}", cert_path);

        if valid {
            println!("\t✅");
        } else {
            println!("\t❌");
        }
    }

    return Ok(());
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        println!("usage: verify_cert_store <store_path>");
        process::exit(1);
    }

    let _engine = Engine::new().expect("Инициализировать движок");

    let store_path = &args[1];

    verify_certificate_store(store_path).expect("Проверить хранилище сертификатов");
}
