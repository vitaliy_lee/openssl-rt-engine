#![allow(clippy::needless_return)]

use std::fmt;
use std::fs;
use std::io;
use std::path::Path;

use libc::c_int;

use openssl::error::ErrorStack;
use openssl::pkcs12::Pkcs12;
use openssl::pkey::{Id, PKey, PKeyCtx, Private};
use openssl::x509::crl::X509CRL;
use openssl::x509::store::{X509Store, X509StoreBuilder, X509StoreRef};
use openssl::x509::verify::{X509VerifyFlags, X509VerifyParam};
use openssl::x509::{X509Ref, X509StoreContext, X509};

use ffi;

pub type Result<T> = std::result::Result<T, ErrorStack>;

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Copy)]
pub enum ParamSet {
    CryptoPro_A,
    CryptoPro_B,
    CryptoPro_C,
    // CryptoPro_XchA,
    // CryptoPro_XchB,
    // TC26_256_A,
    // TC26_256_B,
    // TC26_256_C,
    // TC26_256_D,
    // TC26_512_A,
    // TC26_512_B,
    // TC26_512_C,
}

impl fmt::Display for ParamSet {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = match self {
            Self::CryptoPro_A => "A",
            Self::CryptoPro_B => "B",
            Self::CryptoPro_C => "C",
            // Self::CryptoPro_XchA => "XA",
            // Self::CryptoPro_XchB => "XB",
            // Self::TC26_256_A => "TCA",
            // Self::TC26_256_B => "TCB",
            // Self::TC26_256_C => "TCC",
            // Self::TC26_256_D => "TCD",
            // Self::TC26_512_A => "A",
            // Self::TC26_512_B => "B",
            // Self::TC26_512_C => "C",
        };

        write!(formatter, "{}", text)
    }
}

#[derive(Debug, Clone, Copy)]
pub enum KeyPairAlgorithm {
    GOST3410_2012_256,
    GOST3410_2012_512,
}

impl From<KeyPairAlgorithm> for Id {
    fn from(algorithm: KeyPairAlgorithm) -> Id {
        match algorithm {
            KeyPairAlgorithm::GOST3410_2012_256 => Id::GOST3410_2012_256,
            KeyPairAlgorithm::GOST3410_2012_512 => Id::GOST3410_2012_512,
        }
    }
}

pub struct Engine {
    _engine: openssl::engine::Engine,
}

impl Engine {
    pub fn new() -> Result<Engine> {
        openssl::init();

        let engine_ptr = unsafe {
            cvt(ffi::rt_eng_load_engine())?;
            cvt_p(ffi::rt_eng_get0_engine())?
            // TODO:
            // cvt(ffi::rt_eng_rand_enable(engine_ptr, enable))?;
        };

        let engine = openssl::engine::Engine::from_ptr(engine_ptr);
        engine.set_default(openssl::engine::EngineMethod::ALL)?;

        let rt_engine = Engine { _engine: engine };

        return Ok(rt_engine);
    }

    pub fn generate_private_key(
        &self,
        algorithm: KeyPairAlgorithm,
        param_set: ParamSet,
    ) -> Result<PKey<Private>> {
        let ctx = PKeyCtx::new(algorithm.into())?;
        ctx.keygen_init()?;
        ctx.ctrl_str("paramset", &param_set.to_string())?;
        ctx.keygen()
    }
}

fn cvt(r: c_int) -> Result<c_int> {
    if r <= 0 {
        Err(ErrorStack::get())
    } else {
        Ok(r)
    }
}

fn cvt_p<T>(r: *mut T) -> Result<*mut T> {
    if r.is_null() {
        Err(ErrorStack::get())
    } else {
        Ok(r)
    }
}

pub fn open_fs_trust_store<P: AsRef<Path>>(
    store_path: P,
) -> std::result::Result<X509Store, Box<dyn std::error::Error>> {
    let mut store_builder = X509StoreBuilder::new()?;

    let store_path = store_path.as_ref();

    let cacerts = fs::read_dir(store_path.join("cacerts"))?;

    for entry in cacerts {
        let cert_path = entry?.path();
        // println!("Загрузка 📄 {:?}", cert_path);
        let certificate = open_certificate(cert_path)?;
        store_builder.add_cert(&certificate)?;
    }

    match fs::read_dir(store_path.join("intermediatecerts")) {
        Ok(intermediatecerts) => {
            for entry in intermediatecerts {
                let cert_path = entry?.path();
                // println!("Загрузка 📄 {:?}", cert_path);
                let certificate = open_certificate(cert_path)?;
                store_builder.add_cert(&certificate)?;
            }
        }
        Err(ref error) if error.kind() == io::ErrorKind::NotFound => {}
        Err(error) => return Err(error.into()),
    }

    let crls = fs::read_dir(store_path.join("crls"))?;

    for entry in crls {
        let crl_path = entry?.path();
        // println!("Загрузка 🚫 {:?}", crl_path);
        let crl = open_crl(crl_path)?;
        store_builder.add_crl(&crl)?;
    }

    let store = store_builder.build();

    return Ok(store);
}

pub fn verify_certificate(
    trust_store: &X509StoreRef,
    certificate: &X509Ref,
) -> std::result::Result<bool, Box<dyn std::error::Error>> {
    let flags = X509VerifyFlags::CRL_CHECK
        | X509VerifyFlags::CRL_CHECK_ALL
        | X509VerifyFlags::X509_STRICT
        | X509VerifyFlags::EXTENDED_CRL_SUPPORT;

    // let now = SystemTime::now();
    // let check_time = now.checked_add(Duration::from_secs(10*365*24*60*60)).unwrap();

    let mut param = X509VerifyParam::new()?;
    param.set_flags(flags)?;
    // param.set_time(check_time);
    param.set_depth(2);

    let mut context = X509StoreContext::new()?;

    // println!("{:?}", certificate.not_before());
    // println!("{:?}", certificate.not_after());

    context.init(trust_store, certificate, None)?;
    context.set_param(param);

    context.verify_cert().map_err(|err| err.into())
}

pub fn open_private_key<P: AsRef<Path>>(
    path: P,
) -> std::result::Result<PKey<Private>, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    PKey::private_key_from_pem(&data).map_err(|err| err.into())
}

pub fn open_certificate<P: AsRef<Path>>(
    path: P,
) -> std::result::Result<X509, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    X509::from_pem(&data).map_err(|err| err.into())
}

pub fn open_pkcs12<P: AsRef<Path>>(
    path: P,
) -> std::result::Result<Pkcs12, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    Pkcs12::from_der(&data).map_err(|err| err.into())
}

pub fn open_crl<P: AsRef<Path>>(
    path: P,
) -> std::result::Result<X509CRL, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    X509CRL::from_pem(&data).map_err(|err| err.into())
}
